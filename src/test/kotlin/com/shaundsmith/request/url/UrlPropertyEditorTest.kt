package com.shaundsmith.request.url

import org.assertj.core.api.Assertions
import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class UrlPropertyEditorTest {

    private val anyInvalidString = "any-invalid-string"
    private val aHostUrl = HostUrl(Protocol.HTTP, "host-domain", 30)
    private val aUrl = Url(aHostUrl, "path", "segment")

    @Test fun shouldThrowAnException_WhenTheProvidedStringIsNotAUrl() {
        val urlPropertyEditor = UrlPropertyEditor()

        val theException = Assertions.catchThrowable { urlPropertyEditor.asText = anyInvalidString }

        then(theException)
                .hasMessage("Invalid URL '$anyInvalidString'")
    }

    @Test fun shouldParseTheProvidedString_WhenTheStringIsAUrl() {
        val urlPropertyEditor = UrlPropertyEditor()

        urlPropertyEditor.asText = aUrl.toString()

        then(urlPropertyEditor.value)
                .isEqualTo(aUrl)
    }

    @Test fun shouldUseTheDefaultProtocolPort_WhenNoPortIsProvidedInTheUrl() {
        val urlPropertyEditor = UrlPropertyEditor()

        urlPropertyEditor.asText = "http://localhost"

        val thePropertyValue: Url = urlPropertyEditor.value as Url
        then(thePropertyValue.hostUrl.port)
                .isEqualTo(Protocol.HTTP.defaultPort)
    }

    @Test fun shouldSetTheValueToNull_WhenAnEmptyStringIsProvided() {
        val urlPropertyEditor = UrlPropertyEditor()

        urlPropertyEditor.asText = ""

        val thePropertyValue: Url? = urlPropertyEditor.value as Url?
        then(thePropertyValue)
                .isNull()
    }

}