package com.shaundsmith.request.url

import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class UrlTest {

    @Test fun shouldCreateAUrlWithoutAPath_WhenNoPathIsProvided() {
        val theHostUrl = HostUrl(Protocol.HTTP, "127.0.0.1", 1234)
        val theUrl = Url(theHostUrl)

        then(theUrl.toString())
                .isEqualTo("http://127.0.0.1:1234/")
    }

    @Test fun shouldCreateAUrlWithAPath_WhenPathSegmentsAreProvided() {
        val theHostUrl = HostUrl(Protocol.HTTP, "127.0.0.1", 1234)
        val theUrl = Url(theHostUrl, "part-1", "part-2")

        then(theUrl.toString())
                .isEqualTo("http://127.0.0.1:1234/part-1/part-2")
    }



}