package com.shaundsmith.request.url

import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class HostUrlTest {

    @Test fun isLocalHostShouldReturnTrue_WhenTheHostIsLocalHost() {
        val theLocalHostUrl = HostUrl(Protocol.HTTPS, "localhost", 1234)

        then(theLocalHostUrl.isLocalHost())
                .isTrue()
    }

    @Test fun isLocalHostShouldReturnTrue_WhenTheHostIs127001() {
        val theLocalHostUrl = HostUrl(Protocol.HTTPS, "127.0.0.1", 1234)

        then(theLocalHostUrl.isLocalHost())
                .isTrue()
    }

    @Test fun isLocalHostShouldReturnFalse_WhenTheHostIsNotALocalHostUrl() {
        val theLocalHostUrl = HostUrl(Protocol.HTTPS, "not-local-host", 1234)

        then(theLocalHostUrl.isLocalHost())
                .isFalse()
    }

    @Test fun shouldFormatTheHostUrlAsAUrl() {
        val theLocalHostUrl = HostUrl(Protocol.HTTPS, "host-details", 1234)

        then(theLocalHostUrl.toString())
                .isEqualTo("https://host-details:1234/")
    }

}