package com.shaundsmith.request.url

import org.assertj.core.api.Assertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class HostUrlParserTest {

    private val anyInvalidString = "any-invalid-string"
    private val aHostUrl = HostUrl(Protocol.HTTP, "host-domain", 30)

    @Test fun shouldThrowAnException_WhenTheProvidedStringIsNotAHostUrl() {
        val hostUrlParser = HostUrlParser()

        val theException = catchThrowable { hostUrlParser.parse(anyInvalidString) }

        then(theException)
                .hasMessage("Invalid host URL '$anyInvalidString'")
    }

    @Test fun shouldParseTheProvidedString_WhenTheStringIsAHostUrl() {
        val hostUrlParser = HostUrlParser()

        val parsedHostUrl = hostUrlParser.parse(aHostUrl.toString())

        then(parsedHostUrl)
                .isEqualTo(aHostUrl)
    }

    @Test fun shouldUseTheDefaultProtocolPort_WhenNoPortIsProvidedInTheHostUrl() {
        val hostUrlParser = HostUrlParser()

        val parsedHostUrl = hostUrlParser.parse("http://localhost")

        then(parsedHostUrl?.port)
                .isEqualTo(Protocol.HTTP.defaultPort)
    }

    @Test fun shouldSetTheValueToNull_WhenAnEmptyStringIsProvided() {
        val hostUrlParser = HostUrlParser()

        val parsedHostUrl = hostUrlParser.parse("")

        then(parsedHostUrl)
                .isNull()
    }


}