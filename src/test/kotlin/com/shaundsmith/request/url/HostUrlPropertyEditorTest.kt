package com.shaundsmith.request.url

import org.assertj.core.api.Assertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class HostUrlPropertyEditorTest {

    private val anyInvalidString = "any-invalid-string"
    private val aHostUrl = HostUrl(Protocol.HTTP, "host-domain", 30)

    @Test fun shouldThrowAnException_WhenTheProvidedStringIsNotAHostUrl() {
        val hostUrlPropertyEditor = HostUrlPropertyEditor()

        val theException = catchThrowable { hostUrlPropertyEditor.asText = anyInvalidString }

        then(theException)
                .hasMessage("Invalid host URL '$anyInvalidString'")
    }

    @Test fun shouldParseTheProvidedString_WhenTheStringIsAHostUrl() {
        val hostUrlPropertyEditor = HostUrlPropertyEditor()

        hostUrlPropertyEditor.asText = aHostUrl.toString()

        then(hostUrlPropertyEditor.value)
                .isEqualTo(aHostUrl)
    }

    @Test fun shouldUseTheDefaultProtocolPort_WhenNoPortIsProvidedInTheHostUrl() {
        val hostUrlPropertyEditor = HostUrlPropertyEditor()

        hostUrlPropertyEditor.asText = "http://localhost"

        val thePropertyValue: HostUrl = hostUrlPropertyEditor.value as HostUrl
        then(thePropertyValue.port)
                .isEqualTo(Protocol.HTTP.defaultPort)
    }

    @Test fun shouldSetTheValueToNull_WhenAnEmptyStringIsProvided() {
        val hostUrlPropertyEditor = HostUrlPropertyEditor()

        hostUrlPropertyEditor.asText = ""

        val thePropertyValue: HostUrl? = hostUrlPropertyEditor.value as HostUrl?
        then(thePropertyValue)
                .isNull()
    }


}