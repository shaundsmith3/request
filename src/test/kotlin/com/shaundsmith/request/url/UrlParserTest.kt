package com.shaundsmith.request.url

import org.assertj.core.api.Assertions
import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class UrlParserTest {

    private val anyInvalidString = "any-invalid-string"
    private val aHostUrl = HostUrl(Protocol.HTTP, "host-domain", 30)

    @Test
    fun shouldThrowAnException_WhenTheProvidedStringIsNotAUrl() {
        val urlParser = UrlParser()

        val theException = Assertions.catchThrowable { urlParser.parse(anyInvalidString) }

        then(theException)
                .hasMessage("Invalid URL '$anyInvalidString'")
    }

    @Test
    fun shouldParseTheProvidedString_WhenTheStringIsAUrl_AndNoPathIsProvided() {
        val aUrl = Url(aHostUrl)
        val urlParser = UrlParser()

        val parsedUrl = urlParser.parse(aUrl.toString())

        then(parsedUrl)
                .isEqualTo(aUrl)
    }

    @Test
    fun shouldParseTheProvidedString_WhenTheStringIsAUrl_AndAIsProvided() {
        val aUrl = Url(aHostUrl, "any", "path")
        val urlParser = UrlParser()

        val parsedUrl = urlParser.parse(aUrl.toString())

        then(parsedUrl)
                .isEqualTo(aUrl)
    }

    @Test
    fun shouldUseTheDefaultProtocolPort_WhenNoPortIsProvidedInTheUrl_AndNoPathIsProvided() {
        val urlParser = UrlParser()

        val parsedUrl = urlParser.parse("http://localhost")

        then(parsedUrl?.hostUrl?.port)
                .isEqualTo(Protocol.HTTP.defaultPort)
    }

    @Test
    fun shouldUseTheDefaultProtocolPort_WhenNoPortIsProvidedInTheUrl_AndAPathIsProvided() {
        val urlParser = UrlParser()

        val parsedUrl = urlParser.parse("http://localhost/any-path/")

        then(parsedUrl?.hostUrl?.port)
                .isEqualTo(Protocol.HTTP.defaultPort)
    }

    @Test
    fun shouldSetTheValueToNull_WhenAnEmptyStringIsProvided() {
        val urlParser = UrlParser()

        val parsedUrl = urlParser.parse("")

        then(parsedUrl)
                .isNull()
    }



}