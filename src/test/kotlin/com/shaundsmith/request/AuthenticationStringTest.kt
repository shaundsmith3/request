package com.shaundsmith.request

import org.assertj.core.api.BDDAssertions.then
import org.testng.annotations.Test

class AuthenticationStringTest {

    private val anyUsername: String = "any-username"
    private val anyAuthenticationToken = "any-auth-token"

    @Test fun shouldFormatAuthenticationString() {
        val anAuthenticationString = AuthenticationString(anyUsername, anyAuthenticationToken)

        then(anAuthenticationString.toString()).isEqualTo("$anyUsername:$anyAuthenticationToken")
    }

}