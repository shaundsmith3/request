package com.shaundsmith.request

import org.assertj.core.api.BDDAssertions.then
import org.springframework.http.HttpStatus
import org.testng.annotations.Test

class RequestParametersTest {

    private val anyHttpStatus = HttpStatus.BAD_GATEWAY
    private val anyBody = "Any HTTP response body"

    @Test fun shouldCreateExceptionMessage_FromResponseBodyAndHttpStatus() {
        val anException = RequestResponseException(anyHttpStatus, anyBody)

        then(anException)
                .hasMessage("Received erroneous response from request. Received code: ${anyHttpStatus.value()} and body: $anyBody")
    }

    @Test fun shouldReturnTheHttpStatus() {
        val anException = RequestResponseException(anyHttpStatus, anyBody)

        then(anException.httpStatus).isEqualTo(anyHttpStatus)
    }

    @Test fun shouldReturnTheResponseBody() {
        val anException = RequestResponseException(anyHttpStatus, anyBody)

        then(anException.responseBody).isEqualTo(anyBody)
    }

}