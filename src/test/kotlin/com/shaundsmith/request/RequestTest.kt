package com.shaundsmith.request

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argForWhich
import com.nhaarman.mockitokotlin2.check
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.shaundsmith.request.url.HostUrl
import com.shaundsmith.request.url.Protocol
import com.shaundsmith.request.url.Url
import org.assertj.core.api.Assertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.Serializable
import java.util.Base64
import java.util.Collections

class RequestTest {

    private val anyResponse = "any response"
    private val anyUrl = Url(HostUrl(Protocol.HTTP, "any-host", 80))
    private val anyHttpMethod = HttpMethod.POST
    private val anyRequestParameters = RequestParameters(Collections.emptyMap(), null)

    private lateinit var restTemplate: RestTemplate

    @BeforeMethod fun init() {
        restTemplate = mock {
            on { exchange(any<String>(), any(), any<HttpEntity<Serializable>>(), any<Class<String>>()) } doReturn ResponseEntity(anyResponse, HttpStatus.OK)
            on { exchange(any<String>(), any(), any<HttpEntity<Serializable>>(), any<ParameterizedTypeReference<String>>()) } doReturn ResponseEntity(anyResponse, HttpStatus.OK)
        }
    }

    @AfterMethod fun tearDown() {
        reset(restTemplate)
    }

    @Test fun shouldUseTheProvidedHttpMethod_WhenSendingTheRequest() {
        val theHttpMethod = HttpMethod.PATCH

        val theRequest = Request(anyUrl, theHttpMethod, anyRequestParameters, null, String::class.java, restTemplate)
        theRequest.send()

        verify(restTemplate).exchange(any<String>(), eq(theHttpMethod), any<HttpEntity<Serializable>>(), any<Class<String>>())
    }

    @Test fun shouldUseTheProvidedUrl_WhenSendingTheRequest() {
        val theUrl = Url(HostUrl(Protocol.HTTPS, "hostname", 123), "first", "second")

        val theRequest = Request(theUrl, anyHttpMethod, anyRequestParameters, null, String::class.java, restTemplate)
        theRequest.send()

        verify(restTemplate).exchange(eq(theUrl.toString()), any(), any<HttpEntity<Serializable>>(), any<Class<String>>())
    }

    @Test fun shouldUseTheUrlParametersInTheUrl_WhenSendingTheRequest() {
        val theUrlParameters = RequestParameters(Collections.singletonMap("param1", "value1"))

        val theRequest = Request(anyUrl, anyHttpMethod, theUrlParameters, null, String::class.java, restTemplate)
        theRequest.send()

        verify(restTemplate)
                .exchange(anyUrlWithParameter("param1", "value1"), any(), any<HttpEntity<Serializable>>(), any<Class<String>>())
    }

    @Test fun shouldSendThBodyParameters_WhenSendingTheRequest() {
        val theBodyParameters = RequestParameters(Collections.emptyMap(), "body-value")

        val theRequest = Request(anyUrl, anyHttpMethod, theBodyParameters, null, String::class.java, restTemplate)
        theRequest.send()

        verify(restTemplate)
                .exchange(any<String>(), any(), argForWhich { body == "body-value" }, any<Class<String>>())
    }

    @Test fun shouldUseAParameterizedTypeReference_WhenAParameterizedTypeIsProvided() {
        val theRequest = Request(anyUrl, anyHttpMethod, anyRequestParameters, object: ParameterizedTypeReference<String>() {}, null, restTemplate)
        theRequest.send()

        verify(restTemplate)
                .exchange(any<String>(), any(), any(), any<ParameterizedTypeReference<String>>())
    }

    @Test(dataProvider = "nonHttp200Codes") fun shouldThrowAnException_WhenTheResponseCodeIsNon20x(httpStatus: HttpStatus) {
        whenever(restTemplate.exchange(any<String>(), any(), any<HttpEntity<Serializable>>(), any<Class<String>>()))
                .doReturn(ResponseEntity(anyResponse, httpStatus))

        val theRequest = Request(anyUrl, anyHttpMethod, anyRequestParameters, null, String::class.java, restTemplate)
        val theException = catchThrowable { theRequest.send() }

        then(theException)
                .isInstanceOf(RequestResponseException::class.java)
    }

    @Test(dataProvider = "http200Codes") fun shouldReturnTheResponse_WhenTheResponseCodeIs20x(httpStatus: HttpStatus) {
        whenever(restTemplate.exchange(any<String>(), any(), any<HttpEntity<Serializable>>(), any<Class<String>>()))
                .doReturn(ResponseEntity(anyResponse, httpStatus))

        val theRequest = Request(anyUrl, anyHttpMethod, anyRequestParameters, null, String::class.java, restTemplate)
        val theResponse = theRequest.send()

        then(theResponse).isEqualTo(anyResponse)
    }

    @Test fun shouldAddTheCredentialsToTheHeader_WhenSendingTheRequest() {
        val theCredentials = AuthenticationString("user", "token")

        val theRequest = Request(anyUrl, anyHttpMethod, anyRequestParameters, null, String::class.java, restTemplate)
                .withCredentials(theCredentials)
        theRequest.send()

        verify(restTemplate)
                .exchange(any<String>(), any(), httpHeaderContaining(theCredentials), any<Class<String>>())
    }

    @DataProvider fun nonHttp200Codes(): Array<Array<Any>> {
        return HttpStatus.values()
                .filter { x -> !x.is2xxSuccessful }
                .map { x -> arrayOf(x as Any) }
                .toTypedArray()
    }

    @DataProvider fun http200Codes(): Array<Array<Any>> {
        return HttpStatus.values()
                .filter { x -> x.is2xxSuccessful }
                .map { x -> arrayOf(x as Any) }
                .toTypedArray()
    }

    private fun anyUrlWithParameter(key: String, value: String): String {
        return check { then(it).contains("$key=$value") }
    }

    private fun httpHeaderContaining(authenticationString: AuthenticationString): HttpEntity<Serializable> {
        val encodedCredentials = String(Base64.getEncoder().encode(authenticationString.toString().toByteArray()))
        return argForWhich {
            headers[HttpHeaders.AUTHORIZATION]?.get(0).equals("Basic $encodedCredentials")
        }
    }

}