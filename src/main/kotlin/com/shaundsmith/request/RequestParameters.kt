package com.shaundsmith.request

import java.io.Serializable
import java.util.Collections

/**
 * Structure containing the URL and Body parameters for a request.
 *
 * @param urlParameters URL parameters
 * @param bodyParameters body parameters
 */
internal data class RequestParameters(val urlParameters: Map<String, Any?> = Collections.emptyMap(), val bodyParameters: Serializable? = null)