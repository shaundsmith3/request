package com.shaundsmith.request

import org.springframework.http.HttpStatus

/**
 * Exception indicating that an invalid response has been returned from a REST request.
 */
class RequestResponseException(val httpStatus: HttpStatus, val responseBody: String) :
        Exception("Received erroneous response from request. Received code: ${httpStatus.value()} and body: $responseBody")