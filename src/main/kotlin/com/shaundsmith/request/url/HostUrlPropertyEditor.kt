package com.shaundsmith.request.url

import java.beans.PropertyEditorSupport
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Property Editor to support transformation of property-file values into {@link HostUrl} values.
 *
 * Supports host URLs in the format `protocol://host:port/` and `protocol://host/`
 */
class HostUrlPropertyEditor : PropertyEditorSupport() {

    private val parser = HostUrlParser()

    override fun setAsText(hostUrlString: String?) {
        value = parser.parse(hostUrlString)
    }

}