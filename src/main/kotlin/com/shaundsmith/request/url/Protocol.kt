package com.shaundsmith.request.url

/**
 * Supported URL Protocols
 *
 * @param defaultPort the default port used by the protocol
 */
enum class Protocol(val defaultPort: Int) {
    HTTP(80),
    HTTPS(443);
}