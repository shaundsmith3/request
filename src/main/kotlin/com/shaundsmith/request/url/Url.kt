package com.shaundsmith.request.url

import java.io.Serializable

/**
 * URL for detailing a full location of a service endpoint
 *
 * @param hostUrl Host URL to use as a base for the URL
 * @param endpoint endpoint on the host to use
 */
data class Url(val hostUrl: HostUrl, val endpoint: String = "") : Serializable {

    /**
     * URL for detailing a full location of a service endpoint.
     * Provides an easy means of creating multi-level endpoints by joining the various endpoints using '/'.
     *
     * @param hostUrl Host URL to use as a base for the URL
     * @param endpoints endpoint on the host to use
     */
    constructor(hostUrl: HostUrl, vararg endpoints: String) : this(hostUrl, endpoints.joinToString("/"))

    override fun toString(): String {
        return "$hostUrl$endpoint"
    }

}