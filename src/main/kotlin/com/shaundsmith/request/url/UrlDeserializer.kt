package com.shaundsmith.request.url

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

/**
 * Jackson deserializer for [Url]s.
 */
class UrlDeserializer : JsonDeserializer<Url?>() {

    private val parser = UrlParser()

    override fun deserialize(jsonParser: JsonParser?, deserializer: DeserializationContext?) : Url? {
        val jsonValue = jsonParser?.text

        return parser.parse(jsonValue)
    }

}