package com.shaundsmith.request.url

import java.beans.PropertyEditorSupport

/**
 * Property Editor to support transformation of property-file values into [Url] values.
 *
 * Supports Urls in the format `protocol://host:port/path` and `protocol://host/path`
 */
class UrlPropertyEditor : PropertyEditorSupport() {

    private val parser = UrlParser()

    override fun setAsText(urlString: String?) {
        value = parser.parse(urlString)
    }

}