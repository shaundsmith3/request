package com.shaundsmith.request.url

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import java.io.Serializable

/**
 * URL to an individual host
 *
 * @param protocol protocol for connecting to the host
 * @param host IP address/DNS name of the host
 * @param port port for connecting to the host
 */
@JsonDeserialize(using = HostUrlDeserializer::class)
@JsonSerialize(using = HostUrlSerializer::class)
data class HostUrl(val protocol: Protocol, val host: String, val port: Int) : Serializable{

    /**
     * Returns true if the host is a localhost URL.
     *
     * Localhost URLs are:
     *   127.0.1,
     *   0.0.0.0,
     *   localhost
     *
     * @return true if the host is a localhost URL, false otherwise
     */
    fun isLocalHost(): Boolean = host == "localhost" || host == "127.0.0.1" || host == "0.0.0.0"

    override fun toString(): String {
        val protocolString = protocol.name.toLowerCase()
        return "$protocolString://$host:$port/"
    }
}