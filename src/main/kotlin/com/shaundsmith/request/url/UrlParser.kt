package com.shaundsmith.request.url

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Parses [Url]s from strings
 */
internal class UrlParser {

    companion object {
        private val URL_PATTERN = Pattern.compile("^(https?)://([a-zA-Z0-9.-]*)(?::(\\d+))?(/[-a-zA-Z0-9._/]*)?$")
    }

    /**
     * Parses the provided string into a [Url].
     *
     * If the string is empty or null, then a null Url is returned.
     *
     * @param string the string to parse as a Url
     *
     * @return the Url represented by the string, or null
     *
     * @throws IllegalArgumentException if the provided string is not a Url
     */
    fun parse(string: String?): Url? {
        val urlMatcher = UrlParser.URL_PATTERN.matcher(string)
        val url: Url?
        if (urlMatcher.find()) {
            val hostUrl = parseHostUrl(urlMatcher)
            val path = urlMatcher.group(4)
            url = buildUrl(hostUrl, path)
        } else if (string == null || string.isEmpty()) {
            url = null
        } else {
            throw IllegalArgumentException("Invalid URL '$string'")
        }
        return url
    }

    private fun parseHostUrl(urlMatcher: Matcher): HostUrl {
        val protocol = Protocol.valueOf(urlMatcher.group(1).toUpperCase())
        val host = urlMatcher.group(2)
        val port = parsePort(urlMatcher, protocol)

        return HostUrl(protocol, host, port)
    }

    private fun buildUrl(hostUrl: HostUrl, path: String?): Url {
        val url: Url
        if (path == null) {
            url = Url(hostUrl)
        } else {
            val formattedPath = if (path.startsWith("/")) path.substring(1) else path
            url = Url(hostUrl, formattedPath)
        }
        return url
    }

    private fun parsePort(urlMatcher: Matcher, protocol: Protocol): Int {
        return if (urlMatcher.group(3) == null) {
            protocol.defaultPort
        } else {
            urlMatcher.group(3).toInt()
        }
    }

}