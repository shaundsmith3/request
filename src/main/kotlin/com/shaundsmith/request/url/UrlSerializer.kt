package com.shaundsmith.request.url

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

/**
 * Jackson Serializer for HostUrls
 */
class UrlSerializer : JsonSerializer<Url?>() {

    override fun serialize(url: Url?, jsonGenerator: JsonGenerator?, serializerProvider: SerializerProvider?) {
        jsonGenerator?.writeString(url.toString())
    }
}