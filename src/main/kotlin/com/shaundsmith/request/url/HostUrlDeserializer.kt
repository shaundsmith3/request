package com.shaundsmith.request.url

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

/**
 * Jackson Deserializer for [HostUrl]s
 */
class HostUrlDeserializer : JsonDeserializer<HostUrl?>() {

    private val parser = HostUrlParser()

    override fun deserialize(jsonParser: JsonParser?, deserializer: DeserializationContext?): HostUrl? {
        val jsonValue = jsonParser?.text

        return parser.parse(jsonValue)
    }

}