package com.shaundsmith.request.url

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Host URL parser for parsing strings into [HostUrls]
 */
internal class HostUrlParser {

    companion object {
        private val HOST_URL_PATTERN = Pattern.compile("^(https?)://([a-zA-Z0-9.-]*)(?::(\\d+))?/?$")
    }

    /**
     * Parses the provided string into a HostUrl.
     *
     * If the provided HostUrl is null (or an empty string), then return a null HostUrl
     * If the HostUrl is not valid, throw an exception
     *
     * @param string string to parse
     *
     * @return parsed host url
     *
     * @throws IllegalArgumentException if the provided string is not a HostUrl
     */
    fun parse(string: String?): HostUrl? {
        val hostUrlMatcher = HOST_URL_PATTERN.matcher(string)
        val hostUrl: HostUrl?
        if (hostUrlMatcher.find()) {
            val protocol = Protocol.valueOf(hostUrlMatcher.group(1).toUpperCase())
            val host = hostUrlMatcher.group(2)
            val port = parsePort(hostUrlMatcher, protocol)
            hostUrl = HostUrl(protocol, host, port)
        } else if (string == null || string.isEmpty()) {
            hostUrl = null
        } else {
            throw IllegalArgumentException("Invalid host URL '$string'")
        }
        return hostUrl
    }

    private fun parsePort(hostUrlMatcher: Matcher, protocol: Protocol): Int {
        return if (hostUrlMatcher.group(3) == null) {
            protocol.defaultPort
        } else {
            hostUrlMatcher.group(3).toInt()
        }
    }

}