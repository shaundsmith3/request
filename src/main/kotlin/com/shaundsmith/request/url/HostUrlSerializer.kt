package com.shaundsmith.request.url

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

/**
 * Jackson Serializer for HostUrls
 */
class HostUrlSerializer : JsonSerializer<HostUrl?>() {

    override fun serialize(hostUrl: HostUrl?, jsonGenerator: JsonGenerator?, serializerProvider: SerializerProvider?) {
        jsonGenerator?.writeString(hostUrl.toString())
    }
}