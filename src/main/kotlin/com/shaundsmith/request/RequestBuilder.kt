package com.shaundsmith.request

import com.shaundsmith.request.url.Url
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.web.client.RestTemplate
import java.io.Serializable
import java.util.Collections
import kotlin.collections.Map

/**
 * Class for building HTTP requests
 *
 * @param restTemplate rest template used for making the requests
 */
class RequestBuilder(private val restTemplate: RestTemplate) {

    /**
     * Creates a GET request.
     *
     * @param url the URL to make the request to
     * @param parameters GET/URL parameters
     * @param responseType expected response type
     *
     * @return the Request configured to make a GET requests
     */
    fun <R> get(url: Url, parameters: Map<String, Any?>, responseType: ParameterizedTypeReference<R>): Request<R> {
        val requestParameters = RequestParameters(parameters, null)
        return Request(url, HttpMethod.GET, requestParameters, responseType, null, restTemplate)
    }

    /**
     * Creates a GET request.
     *
     * @param url the URL to make the request to
     * @param parameters GET/URL parameters
     * @param responseType expected response type
     *
     * @return the Request configured to make a GET requests
     */
    fun <R> get(url: Url, parameters: Map<String, Any?>, responseType: Class<R>): Request<R> {
        val requestParameters = RequestParameters(parameters, null)
        return Request(url, HttpMethod.GET, requestParameters, null, responseType, restTemplate)
    }

    /**
     * Creates a POST request.
     *
     * @param url the URL to make the request to
     * @param parameters the object to send as the POST request body
     * @param responseType the expected response type
     *
     * @return the Request configured to make a POST request
     */
    fun <R> post(url: Url, parameters: Serializable, responseType: ParameterizedTypeReference<R>): Request<R> {
        val requestParameters = RequestParameters(Collections.emptyMap(), parameters)
        return Request(url, HttpMethod.POST, requestParameters, responseType, null, restTemplate)
    }

    /**
     * Creates a POST request.
     *
     * @param url the URL to make the request to
     * @param parameters the object to send as the POST request body
     * @param responseType the expected response type
     *
     * @return the Request configured to make a POST request
     */
    fun <R> post(url: Url, parameters: Serializable, responseType: Class<R>): Request<R> {
        val requestParameters = RequestParameters(Collections.emptyMap(), parameters)
        return Request(url, HttpMethod.POST, requestParameters, null, responseType, restTemplate)
    }

    /**
     * Creates a PATCH request.
     *
     * @param url the URL to make the request to
     * @param parameters the object to send as the PATCH request body
     * @param responseType the expected response type
     *
     * @return the Request configured to make a PATCH request
     */
    fun <R> patch(url: Url, parameters: Serializable, responseType: ParameterizedTypeReference<R>): Request<R> {
        val requestParameters = RequestParameters(Collections.emptyMap(), parameters)
        return Request(url, HttpMethod.PATCH, requestParameters, responseType, null, restTemplate)
    }

    /**
     * Creates a PUT request.
     *
     * @param url the URL to make the request to
     * @param parameters the object to send as the PUT request body
     * @param responseType the expected response type
     *
     * @return the Request configured to make a PUT request
     */
    fun <R> put(url: Url, parameters: Serializable, responseType: ParameterizedTypeReference<R>): Request<R> {
        val requestParameters = RequestParameters(Collections.emptyMap(), parameters)
        return Request(url, HttpMethod.PUT, requestParameters, responseType, null, restTemplate)
    }

    /**
     * Creates a HEAD request.
     *
     * @param url the URL to make the request to
     *
     * @return the Request configured to make a HEAD request
     */
    fun head(url: Url): Request<Void> {
        return Request(url, HttpMethod.HEAD, RequestParameters(), null, Void.TYPE, restTemplate)
    }

}

