package com.shaundsmith.request

import java.io.Serializable

/**
 * Authentication String for Basic Base64 Authentication.
 *
 * @param username username for the authentication string
 * @param authenticationToken authentication token
 */
data class AuthenticationString(val username: String, val authenticationToken: String) : Serializable {
    override fun toString(): String = "$username:$authenticationToken"
}