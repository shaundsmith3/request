package com.shaundsmith.request

import com.shaundsmith.request.url.Url
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.io.Serializable
import java.util.Base64

/**
 * Class for making HTTP JSON requests.
 * Shouldn't be instantiated directly, it should only be instantiated via the builder class.
 *
 * @see RequestBuilder
 *
 * @param url URL to send the request to
 * @param httpMethod HTTP method to use for the request
 * @param requestParameters request parameters to send with the request
 * @param clazz class type of the result object
 * @param restTemplate rest template used for making the request
 */
class Request<R> internal constructor(
        private val url: Url,
        private val httpMethod: HttpMethod,
        private val requestParameters: RequestParameters,
        private val parameterizedClazz: ParameterizedTypeReference<R>? = null,
        private val clazz: Class<R>? = null,
        private val restTemplate: RestTemplate) {

    private val httpHeaders = HttpHeaders()

    /**
     * Sets the Base64 Basic authentication credentials on the HTTP request.
     *
     * @param authenticationString authentication string to use to
     *
     * @return this request object
     */
    fun withCredentials(authenticationString: AuthenticationString): Request<R> {
        val credentials = authenticationString.toString()
        val encodedCredentialsAsBytes = Base64.getEncoder().encode(credentials.toByteArray())
        val encodedCredentials = String(encodedCredentialsAsBytes)

        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic $encodedCredentials")

        return this
    }

    /**
     * Sends the request to the URL and return the deserialized result.
     *
     * If the response is not a successful response (200, 201) then a RequestResponseException is thrown
     *
     * @return the deserialized response
     */
    @Throws(RequestResponseException::class)
    fun send(): R? {
        val fullUrl = buildUrlWithUrlParameters()

        httpHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
        val httpEntity: HttpEntity<Serializable?> = HttpEntity(requestParameters.bodyParameters, httpHeaders)

        val response: ResponseEntity<R> = if (parameterizedClazz == null) {
            restTemplate.exchange(fullUrl, httpMethod, httpEntity, clazz!!)
        } else {
            restTemplate.exchange(fullUrl, httpMethod, httpEntity, parameterizedClazz)
        }

        if (!response.statusCode.is2xxSuccessful) {
            throw RequestResponseException(response.statusCode, response.body?.toString() ?: "null")
        }
        return response.body
    }

    private fun buildUrlWithUrlParameters(): String {
        val parameterMap = LinkedMultiValueMap<String, String>()
        requestParameters.urlParameters.forEach {(k, v) -> parameterMap.add(k, v?.toString() ?: "")}

        val uriComponentsBuilder: UriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(url.toString())
        uriComponentsBuilder.queryParams(parameterMap)
        return uriComponentsBuilder.toUriString()
    }

}